import { Component } from '@angular/core';
import { DriverStanding, Driver } from '../../@test/models';
import { Router, ActivatedRoute } from '@angular/router';
import { ResourcesService } from '../../@test/resources';

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent {
  models: DriverStanding[] = [];
  model: Driver[] = [];
  selectedModel: Driver;
  cols: any[];
  fields: any[];

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private _resources: ResourcesService
  ) {
    this.models = this.activeRoute.snapshot.data['models'].MRData.StandingsTable.StandingsLists[0].DriverStandings;
    this.models.forEach(e => {
      console.log(e);
      let driver = new Driver(e, _resources);
      this.model.push(driver);
    })
  }

  ngOnInit() {

    this.cols = [
      { field: 'fullName', header: 'Name' },
      { field: 'constructorName', header: 'Constructor' },
      { field: 'wins', header: 'Wins' },
    ];
  }

  selected(event) {
    this.selectedModel = event;
    console.log(this.selectedModel);
  }
}
