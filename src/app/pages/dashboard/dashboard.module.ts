import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { TestModule } from '../../@test/test.module';

@NgModule({
  imports: [
    ThemeModule,
    NgxEchartsModule,
    TestModule
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule { }
