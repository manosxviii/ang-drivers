import { GbResources } from './gbResources';

export class AppConfig{
  public static AppConfig = {
    BaseURL : "http://localhost:3000/"
  }

  public static _lang: string;

  public static getLanguageRes(){
    return GbResources;
  }
}

