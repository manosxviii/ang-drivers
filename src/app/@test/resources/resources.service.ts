import { Injectable } from '@angular/core';
import { AppConfig } from './appconfig';

@Injectable()
export class ResourcesService {

  constructor() {
  }

  getDriverResources(){
    return AppConfig.getLanguageRes().DriverResources;
  }

  getConstructorResources(){
    return AppConfig.getLanguageRes().ConstructorResources;
  }

  getHttpMessages() {
    return AppConfig.getLanguageRes().HttpMessages;
  }

  getModelResources(model:string){
    return AppConfig.getLanguageRes()[model];
  }

  getAppConfig() {
    return AppConfig.AppConfig;
  }
}
