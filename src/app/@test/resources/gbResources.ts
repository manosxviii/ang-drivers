export class GbResources {

  public static HttpMessages = {
    generalError: "Σφάλμα.",
    postError: "Αποτυχία εγγραφής.",
    postSuccess: "Η εγγραφή ολοκληρώθηκε επιτυχώς!",
    putError: "Αποτυχία επεξεργασίας.",
    putSuccess: "Η επεξεργασία ολοκληρώθηκε επιτυχώς!",
  }

  public static ConstructorResources = {
    constructorId: "Constructor Id",
    url: "Url",
    name: "Name",
    nationality: "Nationality"
  }

  public static DriverResources = {
    dateOfBirth: "Birthday",
    familyName: "Family Name"
  }
}
