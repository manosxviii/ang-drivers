import { Component, SimpleChange, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { growlService } from '../../services';
import { Subscription } from 'rxjs/Subscription';
import { Message } from 'primeng/primeng';

@Component({
  selector: 'growl',
  template: `<p-growl [(value)]="msgs" [styleClass]="'growl-container'" [sticky] ="false">`,
  styleUrls: ['./growl.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class GrowlComponent {

  @Output() destroy = new EventEmitter();
  self = this;
  msgs: Message[] = [];
  subscription: Subscription;
  message: any;

  constructor(private _growlService: growlService) {
    this.subscription = this._growlService.mgs$
      .subscribe(item => {
        this.handle(item);
        console.log(item);
        this.msgs.push(item);
      });

  }

  handle(item: any) {
    console.log(item);
  }

  ngOnDestroy() {
    // prevent memory leak when component is destroyed
    this.subscription.unsubscribe();

  }
}
