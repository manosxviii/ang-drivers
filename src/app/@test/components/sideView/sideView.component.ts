import { Component, Input } from "@angular/core";

@Component({
  selector: 'ngx-sideView',
  styleUrls: ['./sideView.component.scss'],
  templateUrl: './sideView.component.html',
})

export class SideViewComponent{

  @Input() model: any;
  @Input() fields: any[];
  @Input() resources: any;

  constructor(){
  }
}
