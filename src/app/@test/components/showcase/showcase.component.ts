import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { DatePipe } from '@angular/common';
import { SelectItem, MenuItem, DialogModule } from 'primeng/primeng';


@Component({
  selector: 'ngx-showcase',
  styleUrls: ['./showcase.component.scss'],
  templateUrl: './showcase.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ShowcaseComponent {

  @Input() model: any[];
  @Input() cols: any[];
  @Input() selectionMode: string;
  @Output() onSelected = new EventEmitter<any>();
  columnOptions: SelectItem[];
  selectedModel: any;
  items: MenuItem[];

  constructor() {
  }

  ngOnInit() {
    this.columnOptions = [];
    for (let i = 0; i < this.cols.length; i++) {
      this.columnOptions.push({ label: this.cols[i].header, value: this.cols[i] });
    }
  }

  onRowSelect(e) {
    this.onSelected.emit(this.selectedModel);
  }

  sort(event: any) {
    this.model = this.model.sort((n1, n2) => {
      let i = 0;
      let c1, c2;

      if (isNaN(Number(n1[event['field']])) && isNaN(Number(n2[event['field']]))) {
        c1 = n1[event['field']];
        c2 = n2[event['field']];
      }
      else {
        c1 = Number(n1[event['field']]);
        c2 = Number(n2[event['field']]);
      }

      if (c1 > c2) {
        return 1 * event.order;
      }

      if (c1 < c2) {
        return -1 * event.order;
      }
      return 0;
    })
  }
}


