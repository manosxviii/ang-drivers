import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Message } from 'primeng/primeng';
import { ResourcesService } from '../../resources/resources.service';

@Injectable()
export class growlService {

  private _msgs = new Subject<any>();

  resources: any;

  mgs$ = this._msgs.asObservable();
  constructor(private _resources: ResourcesService) {
    this.resources = _resources.getHttpMessages();
  }

  // service command
  pushMessage(message: string) {
    let t: any;
    t = { severity: 'error', summary: message, detail: 'PrimeNG tttt' };

    setTimeout(() => {
      this._msgs.next(t);
    }, 10);
  }

  httpError(error: any) {
    let errMsg;
    let customException: boolean = typeof error['_body'] === 'string' ? true : false;

    if (customException) {
      let exceptionBody = JSON.parse(error['_body']);
      let msgError = exceptionBody['ExceptionMessage'];
      errMsg = this.resources[msgError];
    }

    if (errMsg === undefined && customException) {
      errMsg = this.resources['generalErrorDetails'];
    }

    if (errMsg === undefined)
      errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    let message: any;
    message = { severity: 'error', summary: this.resources.generalError, detail: errMsg };

    this._msgs.next(message);

    return Observable.throw(errMsg);
  }

  httpPostSuccess(response: any) {
    let message: any;
    message = { severity: 'success', summary: "Success", detail: this.resources.postSuccess };

    setTimeout(() => this._msgs.next(message), 10);
  }

  httpPutSuccess(response: any) {
    let message: any;
    message = { severity: 'success', summary: "Success", detail: this.resources.putSuccess };

    setTimeout(() => this._msgs.next(message), 10);
  }
}
