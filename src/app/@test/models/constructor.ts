import { ResourcesService } from "../resources";

export class Constructor {
  constructorId: string;
  url: string;
  name: string;
  nationality: string;

  fields: any;
  resources: any;

  constructor(input: any, private _resources: ResourcesService) {
    this.constructorId = input.constructorId;
    this.url = input.url;
    this.name = input.name;
    this.nationality = input.nationality;

    this.resources = _resources.getConstructorResources();

    this.fields = [
      { name: 'constructorId' },
      { name: 'url', type: 'link' },
      { name: 'name' },
      { name: 'nationality' }
    ]
  }
}
