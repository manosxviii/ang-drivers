import { Constructor } from "./constructor";
import { DriverStanding } from "./driverStanding";
import { ResourcesService } from "../resources";

export class Driver {
  driverId: string;
  permanentNumber: number;
  code: string;
  url: string;
  givenName: string;
  familyName: string;
  dateOfBirth: Date;
  nationality: string;
  constructorModel: Constructor;
  constructorName: string;
  fullName: string;
  wins: number;

  fields: any;
  resources: any;

  constructor(driverStanding: DriverStanding, private _resources: ResourcesService) {
    this.driverId = driverStanding.Driver.driverId;
    this.permanentNumber = driverStanding.Driver.permanentNumber;
    this.code = driverStanding.Driver.code;
    this.url = driverStanding.Driver.url;
    this.givenName = driverStanding.Driver.givenName;
    this.familyName = driverStanding.Driver.familyName;
    this.dateOfBirth = driverStanding.Driver.dateOfBirth;
    this.nationality = driverStanding.Driver.nationality;
    this.constructorModel = new Constructor(driverStanding.Constructors[0], _resources);
    this.constructorName = driverStanding.Constructors[0].name;
    this.fullName = driverStanding.Driver.givenName + ' ' + driverStanding.Driver.familyName;
    this.wins = driverStanding.wins;

    this.fields = [
      { name: 'dateOfBirth' },
      { name: 'familyName' },
      { name: 'constructorModel', type: 'object' }
    ];

    this.resources = _resources.getDriverResources();
  }
}
