import { Constructor } from "./constructor";
import { Driver } from "./driver";

export class DriverStanding{
    position: number;
    positionText: string;
    points: number;
    wins: number;
    Driver: Driver;
    Constructors: Constructor[];
}
