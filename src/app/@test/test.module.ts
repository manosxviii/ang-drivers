import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../@theme/theme.module';

import {
  GrowlModule,
  DataTableModule,
  SharedModule,
  ContextMenuModule,
  OverlayPanelModule,
  DialogModule,
  InputTextModule
} from 'primeng/primeng';

import {
  HttpService,
  growlService,
  GenericResolve
} from './services';

import {
  ResourcesService
} from './resources';

import {
  GrowlComponent,
  ShowcaseComponent,
  SideViewComponent
} from './components'

const MODULES = [
  GrowlModule,
  DataTableModule,
  SharedModule,
  ContextMenuModule,
  OverlayPanelModule,
  ReactiveFormsModule,
  DialogModule,
  InputTextModule
]

const SERVICES = [
  GenericResolve,
  HttpService,
  growlService,
  ResourcesService
];

const COMPONENTS = [
  GrowlComponent,
  ShowcaseComponent,
  SideViewComponent
];

const MODELS = [
];

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    ...MODULES
  ],
  exports: [
    ...COMPONENTS
  ],

  declarations: [
    ...COMPONENTS
  ],
  providers: [
    // ...SERVICES,
  ],
})
export class TestModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: TestModule,
      providers: [
        ...SERVICES,
        ...MODELS
      ],
    };
  }
}
